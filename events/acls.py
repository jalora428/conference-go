# Anti-Corruption Layer;
# Code to make the HTTP requests to PEXELs and Open Weather Map
# Code should return python dictionary to be used by api_views

from curses import keyname
from urllib import request
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    params = {
        "q": city + "," + state + "," + "1",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    # Make the request
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
        geoDict = {"latitude": latitude, "longitude": longitude}
    except (KeyError, IndexError):
        geoDict = {"latitude": None, "longitude": None}
    # Create the URL for the current weather API with the latitude and longitude
    if geoDict["latitude"] is not None and geoDict["longitude"] is not None:
        params = {
            "lat": geoDict["latitude"],
            "lon": geoDict["longitude"],
            "appid": OPEN_WEATHER_API_KEY,
        }
        url = "https://api.openweathermap.org/data/2.5/weather"
        response = requests.get(url, params=params)
        content = json.loads(response.content)
        try:
            temperature = content["main"]["temp"]
            description = content["weather"][0]["description"]
            weatherDict = {
                "temperature": temperature,
                "description": description,
            }
            return weatherDict
        except (KeyError, IndexError):
            weatherDict = {"temperature": None, "description": None}
            return weatherDict
    else:
        weatherDict = {"temperature": None, "description": None}
        return weatherDict
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
